""" This script simply defines the SlangTerm class, which contains hard-coded rules for changing
a scraped slang term's example sentence into one using only standard English. """

import re as regex

class SlangTerm():
	def __init__(self, slang='', pos='', definition='', example=''):
		definition = definition.lower().replace('"', '').replace(" + ", " and ")
		if "comes from" in definition:
			definition = definition[:definition.index("comes from")]
		if definition.split()[0] in ["a", "an", "the"]:
			definition = ' '.join(definition.split()[1:])
		if "translation:" in definition:
			definition = definition[definition.index("translation:") + 12:]
		if definition.count(",") <= 3 and all(len(commaSplit.split()) <= 3 for commaSplit in definition.split(",")):
			definition = definition.replace(",", ";")

		self.slang = slang
		self.pos = pos
		self.definition = definition
		self.example = example
		self.synonyms = []
		if ";" in definition:
			self.synonyms = list(map(str.lower, map(str.strip, definition.split(";"))))
		if regex.match("term|acronym|short|euphemism for (.+)", definition):
			self.synonyms += [regex.match("term|acronym|short|euphemism for (.+)", definition).groups()[0]]
		if len(definition.split()) == 1:
			self.synonyms += [definition]
		if pos == 'verb' and definition[:3] == "to " and definition.count(",") >= 3:
			self.synonyms += list(map(str.strip, definition[3:].split(",")))
		if " or " in definition and definition.count(",") == 0 and definition[:3] == "to ":
			self.synonyms += definition[3:].split(" or ")

		match = regex.match("^(.+?), (.+?), or (.+)[, ]?.*$", definition)
		if match and all(len(group.split()) <= 2 for group in match.groups()):
			self.synonyms += list(match.groups())

		match = regex.match("variant of (.+?)$", definition)
		if match:
			self.synonyms += list(match.groups()[0].split(",")[0])

		self.synonyms = [syn for syn in self.synonyms if syn and len(syn.strip()) > 1]
		for i, syn in enumerate(self.synonyms):
			if syn[:3] == "or " or syn[:3] == "to ":
				self.synonyms[i] = syn[3:]
			self.synonyms[i] = self.synonyms[i].strip()

		self.translation = translateSentence(self)

def translateSentence(st):
	if st.synonyms:
		return st.example.lower().replace(st.slang.lower(), min(st.synonyms, key=len))

	slang, example, definition = st.slang.lower(), st.example.lower(), st.definition.lower()

	if definition.split()[0] == "to" and st.slang.split()[0] != "to":
		definition = ' '.join(definition.split()[1:])

	if "he" in example.split() and "male" in example.split():
		example = example.replace("male", "")
	if "she" in example.split() and "female" in example.split():
		example = example.replace("female", "")

	return example.replace(slang, definition)