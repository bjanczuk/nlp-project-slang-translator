# CSE 40657
# Bart Janczuk
# Project - Slang Translator

Link to Google Drive folder with data: https://drive.google.com/open?id=1TtNa86ijf8Y12E_nQauwBZ_LiYknIVv5

Here is a pipeline of commands/processes that recreates what I've done here from start to finish:

python3 scrapeSlang.py

->

cat scrapedSlangTerms.txt | head -2000 > 500_input.txt

->

sed -n 0~4p 500_input.txt > 500_correct.txt

->

* Manually read through and change every sentence in 500_correct.txt to be correct. *

->

python3 makeBaselineTranslations.py 500_input.txt 500_baseline.txt

->

python3 combineMovieFiles.py

->

python3 createTrainingFiles.py

->

python3 OpenNMT-py/preprocess.py -train_src incorrect_train.txt  -train_tgt correct_train.txt  -valid_src incorrect_test.txt  -valid_tgt correct_test.txt  -save_data preprocessed_data

->

python3 OpenNMT-py/train.py -data preprocessed_data -save_model rnn-model -train_steps 85000

->

python3 OpenNMT-py/translate.py -model rnn-model_step_85000.pt -src 500_baseline.txt -output rnn-predictions.txt -replace_unk

->

python3 bleu.py 500_baseline