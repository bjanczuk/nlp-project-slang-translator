""" This script creates the training files needed to generate the RNN by artificially introducing
mistakes into lines of movie dialogue. These are found in files written by the combineMovieFiles.py script.
There are 13 such rules hard-coded into this script; for each line, it randomly picks one rule and tries
to apply it to the line. All of these changes are then saved to the incorrect_train.txt and incorrect_test.txt
files, which mirror the correct_train.txt and correct_test.txt files. """

from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.tokenize.moses import MosesDetokenizer
from nltk.stem.wordnet import WordNetLemmatizer
from pattern.en import pluralize, singularize

import random
import string

noun_set = set()

# Replace "a"s with "an"s or vice versa.
def rule1(tokens, tags):
	if "a" in tokens:
		tokens = [t + "n" if t.lower() == "a" else t for t in tokens]
	elif "an" in tokens:
		tokens = [t[0] if t.lower() == "an" else t for t in tokens]
	return tokens

# Replace "is"s with "be"s or vice versa.
def rule2(tokens, tags):
	if "is" in tokens:
		tokens = ["be" if t == "is" else "Be" if t == "Is" else t for t in tokens]
	elif "be" in tokens:
		tokens = ["is" if t == "be" else "Is" if t == "Be" else t for t in tokens]
	return tokens

def rule3(tokens, tags):
	if "is" in tokens:
		tokens = ["are" if t == "is" else "Are" if t == "Is" else t for t in tokens]
	elif "are" in tokens:
		tokens = ["is" if t == "are" else "Is" if t == "Are" else t for t in tokens]
	return tokens

# Change a random singular noun into a plural noun.
def rule4(tokens, tags):
	options = [tag for (token, tag) in tags if tag == 'NN']
	if len(options) == 0: return tokens

	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = -1
	for i, (token, tag) in enumerate(tags):
		if tag == 'NN':
			counter += 1
			if counter == random_index:
				tokens[i] = pluralize(token)
	return tokens

# Change a random plural noun into a singular noun.
def rule5(tokens, tags):
	options = [tag for (token, tag) in tags if tag == 'NNS']
	if len(options) == 0: return tokens

	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = -1
	for i, (token, tag) in enumerate(tags):
		if tag == 'NNS':
			counter += 1
			if counter == random_index:
				tokens[i] = singularize(token)
	return tokens

# Randomly add an "a" in front a plural noun.
def rule6(tokens, tags):
	options = [tag for (token, tag) in tags if tag == 'NNS']
	if len(options) == 0: return tokens

	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = change_index = -1
	for i, (token, tag) in enumerate(tags):
		if tag == 'NNS':
			counter += 1
			if counter == random_index:
				change_index = i

	if change_index > -1:
		tokens = tokens[:change_index] + ["a"] + tokens[change_index:]
	return tokens

# Randomly remove a letter from some word(s).
def rule7(tokens, tags):
	if len(tokens) <= 3: return tokens

	if len(tokens) == 4:
		num_words_to_change = 1
	elif 5 <= len(tokens) <= 7:
		num_words_to_change = 2
	else:
		num_words_to_change = random.randint(2, 4)

	indeces_to_change = random.sample(range(len(tokens)), num_words_to_change)
	for i in indeces_to_change:
		remove_index = random.randint(0, len(tokens[i]) - 1) if len(tokens) > 1 else 0
		tokens[i] = tokens[i][:remove_index] + tokens[i][remove_index+1:]

	return tokens

# Randomly insert a letter into some word(s).
def rule8(tokens, tags):
	if len(tokens) <= 3: return tokens

	if len(tokens) == 4:
		num_words_to_change = 1
	elif 5 <= len(tokens) <= 7:
		num_words_to_change = 2
	else:
		num_words_to_change = random.randint(2, 4)

	indeces_to_change = random.sample(range(len(tokens)), num_words_to_change)
	for i in indeces_to_change:
		insert_index = random.randint(0, len(tokens[i]))
		new_letter = random.choice(string.ascii_lowercase if insert_index > 0 else string.ascii_uppercase)
		tokens[i] = tokens[i][:insert_index] + new_letter + tokens[i][insert_index:]

	return tokens

# Change a random verb into its stem.
def rule9(tokens, tags):
	options = [tag for (token, tag) in tags if len(tag) >= 2 and token[0] != "'" and tag[:2] == "VB"]
	if len(options) == 0: return tokens

	wnl = WordNetLemmatizer()
	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = -1
	for i, (token, tag) in enumerate(tags):
		if len(tag) >= 2 and token[0] != "'" and tag[:2] == "VB":
			counter += 1
			if counter == random_index:
				tokens[i] = wnl.lemmatize(token, "v")
	return tokens

# Add some form of "___ for" in front of a random noun.
def rule10(tokens, tags):
	options = [tag for (token, tag) in tags if tag in ['NN', 'NNS']]
	if len(options) == 0: return tokens

	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = change_index = -1
	for i, (token, tag) in enumerate(tags):
		if tag in ['NN', 'NNS']:
			counter += 1
			if counter == random_index:
				change_index = i

	if change_index > -1:
		new_phrase = random.choice(["acronym for", "acronym for", "acronym for", "name for", "abbreviation for", "alternate name for", "pejorative name for", "of a"]).split()
		tokens = tokens[:change_index] + new_phrase + tokens[change_index:]

	return tokens

# Randomly add "or ___ ___" following a noun.
def rule11(tokens, tags):
	global noun_set
	options = [tag for (token, tag) in tags if tag in ['NN', 'NNS']]
	if len(options) == 0: return tokens

	random_index = random.randint(0, max(len(options) - 1, 0))
	counter = change_index = -1
	for i, (token, tag) in enumerate(tags):
		if tag in ['NN', 'NNS']:
			counter += 1
			if counter == random_index:
				change_index = i

	if change_index > -1:
		in_between_word = random.choice(["a", "be", "to", ""])
		new_phrase = ["or", in_between_word, token if len(noun_set) <= 1 else random.choice([n for n in noun_set if n != tokens[change_index].lower()])]
		tokens = tokens[:change_index+1] + new_phrase + tokens[change_index+1:]

	return tokens

# Convert "he/she/it is ____" to "he/she/it is a thing/person that is ____".
def rule12(tokens, tags):
	if len(tokens) == 1: return tokens

	for i in range(len(tags) - 1):
		token, tag = tags[i]
		if tag in ['NN', 'NNS', 'NNP'] and tags[i+1][0] in ['is', 'are']:
			return tokens[:i+2] + ["a", "thing", "that", tags[i+1][0]] + tokens[i+2:]
		elif tag == 'PRP' and tags[i+1][0] == 'is':
			return tokens[:i+2] + ["a", "person", "who", tags[i+1][0]] + tokens[i+2:]

	return tokens

# Duplicate a random word.
def rule13(tokens, tags):
	random_index = random.randint(0, max(len(tokens) - 1, 0))
	return tokens[:random_index] + [tokens[random_index]] + tokens[random_index:]

rules = [	
			rule1,
			rule2,
			rule3,
			rule4,
			rule5,
			rule6,
			rule7,
			rule8,
			rule9,
			rule10,
			rule11,
			rule12,
			rule13
		]

if __name__ == '__main__':
	dtkn = MosesDetokenizer()

	for correct_path, incorrect_path in [("correct_train.txt", "incorrect_train.txt"), ("correct_test.txt", "incorrect_test.txt")]:
		with open(correct_path) as correctFile, open(incorrect_path, "w+") as incorrectFile:
			same = total = 0
			for count, line in enumerate(map(str.strip, correctFile.readlines())):
				if count % 25000 == 0: print(count)

				tokens = word_tokenize(line)
				tags = pos_tag(tokens)
				correct_detokenized = dtkn.detokenize(tokens, return_str=True)

				noun_set |= {token.lower() for (token, tag) in tags if tag in ["NN", "NNS"]}

				# Try applying a rule 3 times in order to increase the number of sentences that end up modified.
				for _ in range(3):
					random_rule = random.choice(rules)
					new_tokens = random_rule(tokens, tags)
					incorrect_detokenized = dtkn.detokenize(new_tokens, return_str=True)
					if correct_detokenized != incorrect_detokenized: break

				if correct_detokenized == incorrect_detokenized: same += 1
				total += 1
				incorrectFile.write(incorrect_detokenized + "\n")

		print("{} lines ended up not changing out of {} total.".format(same, total))
