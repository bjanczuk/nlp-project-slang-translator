""" This script scrapes slang terms from onlineslangdictionary.com indefinitely until the user terminates with
Ctrl-C. These slang terms are saved to scrapedSlangTerms.txt in the format needed for any input file used by
the makeBaselineTranslations.py script. """

from slangterm import SlangTerm
import requests
import re as regex
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet as wn

def cleanUpText(text):
	text = text.strip()
	if "i.e." in text:
		text = text[:text.index("i.e.")]
	if ", especially" in text:
		text = text[:text.index(", especially")]
	if "(" in text:
		text = text[:text.index("(")]
	text = text.split(".")[0]
	text = text.replace("&#39;", "'")
	text = text.replace("&lt;", "<").replace("&gt;", ">")
	text = text.replace("&quot;", '"')
	text = text.replace("and/or", "or")
	text = text.replace("(one's)", "one's")
	text = text.replace(" usually ", " ")
	text = text.replace(", etc", "").replace("; etc", "")
	text = regex.sub("""<a href=.+?>(.+?)</a>""", r"\1", text)
	text = regex.sub("""<.+?>""", "", text)
	text = regex.sub("""\((.+?)\)""", r"\1", text)
	if text[-5:] == ", the":
		text = "the " + text[:-5]
	if any([pos == text.split()[0] for pos in ["adjective", "noun", "verb", "other"]]):
		text = text.split()[0]
	return text.strip(",")

def lemmatizeVerbs(sentence, slang):
	lemmatized = []
	wnl = WordNetLemmatizer()
	for word in sentence.split():
		if wn.synsets(word) and wn.synsets(word)[0].pos() == 'v' and wnl.lemmatize(word, 'v') in slang.split():
			lemmatized.append(wnl.lemmatize(word, 'v'))
		else:
			lemmatized.append(word)

	return ' '.join(lemmatized)

if __name__ == "__main__":
	slangTerms = dict()

	with open("scrapedSlangTerms.txt", "w+") as slangFile:
		print("Starting scraping...")

		while True:
			r = requests.get('http://onlineslangdictionary.com/random-word/')
			slangDict = dict()

			patternSlang = """.+?What does (.+?) mean?.+?"""
			patternRest = """.*?<h3><b>(.+?)</b></h3><ul><li>(.+?)<blockquote class="sentence">(.+?)</blockquote>"""
			censored = '<span class="censored">'

			slangMatch = regex.match(patternSlang, ' '.join(r.text.split("\n")))
			restMatch = regex.match(patternRest, ''.join(r.text.split("\n")))
			if not (slangMatch and restMatch) or censored in restMatch.groups()[2] \
				or restMatch.groups()[1][:4] == "see " or len(restMatch.groups()[1].split()) > 20 \
				or any([invalidWord in restMatch.groups()[1] for invalidWord in ["general", "random"]]): continue

			cleanedTexts = list(map(cleanUpText, [slangMatch.groups()[0], restMatch.groups()[0], restMatch.groups()[1], restMatch.groups()[2]]))
			cleanedTexts[3] = lemmatizeVerbs(cleanedTexts[3], cleanedTexts[0])
			st = SlangTerm(*cleanedTexts)
			if st.slang not in st.example or st.slang in slangTerms or not st.slang or not st.definition: continue

			slangFile.write("\n".join([st.slang, st.pos, st.definition, st.example]) + "\n")
			slangTerms[st.slang] = st
			if len(slangTerms) % 100 == 0: print("\t\t\t...scraped {} terms so far".format(len(slangTerms)))