""" This script just iterates over every slang term found in the given input file and creates
a SlangTerm object out of it. This generates a baseline translation from the slang term's example
sentences into one without slang, which then gets appended to the baseline.txt file. """

import sys
from slangterm import SlangTerm

if __name__ == '__main__':
	if len(sys.argv) != 3:
		sys.exit("usage: python3 makeBaselineTranslations.py INPUT_FILE.txt BASELINE.txt")
	
	try:
		with open(sys.argv[1]) as file:
			lines = file.readlines()
	except FileNotFoundError:
		sys.exit("ERROR: {} not found".format(sys.argv[1]))

	with open(sys.argv[2], "w+") as baselineFile:
		for i in range(0, len(lines), 4):
			term = lines[i]
			pos = lines[i+1]
			definition = lines[i+2]
			example = lines[i+3]

			st = SlangTerm(term, pos, definition, example)
			baselineFile.write(st.translation.strip() + "\n")
