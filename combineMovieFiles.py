""" This script opens the ./dialogs/ directory, iterates over every directory contained there, and then iterates over each
.txt file contained within the inner directory. For each of these .txt files, this script cleans the data a bit and then
appends all useful lines to the correct_train.txt or correct_test.txt files (to be used for developing the RNN model). """

import os
import random
import re
import string
import sys

def lineStartsAllCaps(line):
	return all([c.isupper() for c in line.split()[0]]) and len(line.split()[0]) > 1

if __name__ == '__main__':
	valid_chars = set(string.ascii_letters + string.digits + string.whitespace + string.punctuation)
	punctuation = set(string.punctuation)
	letters = set(string.ascii_letters)
	base_path = "./dialogs/"

	with open("correct_train.txt", "w+") as trainingFile, open("correct_test.txt", "w+") as testingFile:
		total_lines = []

		for directory in os.listdir(base_path):
			if os.path.isdir(base_path + directory):
				print("STARTING DIRECTORY: {}".format(directory))

				for textFile in os.listdir(base_path + directory):
					if os.path.isfile(base_path + directory + "/" + textFile):
						print("\t\t\t\t\t{}".format(textFile))
						
						with open(base_path + directory + "/" + textFile) as script:
							lines = [line.strip() for line in script.readlines() if len(line.strip()) > 0][10:]

							while lines and not lineStartsAllCaps(lines[0]):
								lines = lines[1:]
								
							if not lines: continue

							dialog, sentence_list = [], []
							for line in lines:
								if lineStartsAllCaps(line):
									if len(sentence_list) > 0:
										sentence = " ".join(sentence_list)
										if sentence[-1] in punctuation:
											sentence = re.sub(r'\([^)]*\)', '', sentence)
											sentence = re.sub(r'[A-Z]{3,}', '', sentence)
											sentence = sentence.strip().replace("  ", " ").replace("?", "=").replace("!", "=").replace(". ", "= ")
											for sentence_split in sentence.split("="):
												if len(sentence_split.split()) >= 5 and re.findall(" [ ]+", sentence_split) == [] and \
												 sentence_split[0] in letters and sentence_split[-1] != "," and not lineStartsAllCaps(sentence_split) \
												 and "--" not in sentence_split:
													dialog.append(sentence_split.strip().lower())
										sentence_list = []
								else:
									sentence_list.append(line)

							total_lines.extend(dialog)

		random.shuffle(total_lines)
		train_size = int(len(total_lines) * 0.8)
		for line in total_lines[:train_size]:
			trainingFile.write(line + "\n")

		for line in total_lines[train_size:]:
			testingFile.write(line + "\n")
