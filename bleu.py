""" This script simply opens each of the three input files and computes the average BLEU score comparing
each of the predictions against their correct translation. """

import sys
from slangterm import SlangTerm
from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import SmoothingFunction

if __name__ == '__main__':
	if len(sys.argv) != 4:
		sys.exit("usage: python3 bleu.py BASELINE.txt RNN_PREDICTIONS.txt CORRECT.txt")

	lines = []
	for filepath in sys.argv[1:]:
		try:
			with open(filepath) as file:
				lines.append(file.readlines())
		except FileNotFoundError:
			sys.exit("ERROR: {} not found".format(filepath))

	baselines, predictions, corrections = lines
	if len(predictions) != len(corrections) or len(predictions) != len(baselines):
		sys.exit("ERROR: lengths of input files must be equal")

	scores = []
	for (base, pred, corr) in [(baselines[index], predictions[index], corrections[index]) for index in range(len(baselines))]:
		if len(pred.strip().split()) <= 5: pred = base
		score = sentence_bleu([corr], pred, smoothing_function=SmoothingFunction().method3, weights=(0.2,0.2,0.2,0.2,0.2))
		scores.append(score)

	print("BLEU score average: {:.2%}".format(sum(scores) / float(len(scores))))